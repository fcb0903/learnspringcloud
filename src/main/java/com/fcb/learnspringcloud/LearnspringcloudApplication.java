package com.fcb.learnspringcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnspringcloudApplication {

    public static void main(String[] args) {
        SpringApplication.run(LearnspringcloudApplication.class, args);
    }
}
